#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup() {
	//ofSetBackgroundAuto(false);
	
}

//--------------------------------------------------------------
void ofApp::update() {

}

//--------------------------------------------------------------
void ofApp::draw() {
	// Set background color
	ofBackground(0, 0, 0);

	// Calculate the radius of the circle thats being drawn
	radius = 2 * (sqrt(pow((endX - startX), 2) + pow((endY - startY), 2)));
	
	// Lets me know that the radius is being recorded
	cout << "radius is " << radius << endl;

	// If the mouse is pressed, a circle is actively following the mouse as its drawn
	if (ofGetMousePressed) {
		ofDrawEllipse(startX, startY, radius, radius);
	}

	// Draw colored rectangles to represent each color you can draw with 
		// doesnt work right
	//	ofSetColor(255, 0, 0);
		ofDrawRectangle(0, 700, 300, 100);
	//	ofSetColor(0, 255, 0);
		ofDrawRectangle(350, 700, 300, 100);
	//	ofSetColor(0, 0, 255);
		ofDrawRectangle(700, 700, 300, 100);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key) {

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {
	// Records the end of the radius
	endX = x;
	endY = y;
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {
	// If the mouse is clicked inside of a color box, then the color is changed
	if (ofGetMouseY() >= 700 && ofGetMouseY() <= 800) {
		startX = 0;
		startY = 0;
		if (ofGetMouseX() >= 0 && ofGetMouseX() <= 300) {
			ofSetColor(255, 0, 0);
		}
		else if (ofGetMouseX() >= 350 && ofGetMouseX() <= 650) {
			ofSetColor(0, 255, 0);
		}
		else if (ofGetMouseX() >= 700 && ofGetMouseX() <= 1000) {
			ofSetColor(0, 0, 255);
		}

	}
	// If mouse is not inside color box, starting location is recorded
	else {
		startX = x;
		startY = y;
	}
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
	// Records the end location as the mouse is released
	endX = x;
	endY = y;

	// Outputs end x so I know its working 
	cout << "end x is " << endX << endl;
	
	
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
